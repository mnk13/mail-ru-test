# Test assignment for Mail.ru

[Test assignment description](Test-assignment-PHP.pdf) (in Russian)

## Requirements
* PHP 7.2

## How to run

```
cd web
php -S localhost:8000
```

## Endpoints

### GET /search

#### Params

* **priceFrom** *int* Thousands roubles
* **priceTo** *int* Thousands roubles
* **roomsNumberFrom** *int* 
* **roomsNumberTo** *int*
* **subwayStationIds** *int[]* Ids can be retrieved by `/subway-stations` endpoint

Request example
```
/search?priceFrom=1000&priceTo=3000&subwayStationIds[]=500&subwayStationIds[]=1000
``` 

### GET /subway-stations

No params
