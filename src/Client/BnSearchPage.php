<?php

declare(strict_types=1);

namespace Bn\Client;

use Symfony\Component\DomCrawler\Crawler;

class BnSearchPage
{
    /** @var string */
    private $html;

    /** @var string */
    private $url;

    /** @var Crawler|null */
    private $crawler;

    public function __construct(string $html, string $url)
    {
        $this->html = $html;
        $this->url = $url;
    }

    private function crawler(): Crawler
    {
        if (null === $this->crawler) {
            $this->crawler = new Crawler($this->html, $this->url);
        }

        return $this->crawler;
    }

    /**
     * @return BnSearchResultRow[]
     */
    public function getSearchRows(): array
    {
        return $this->crawler()
            ->filter('div.catalog_result div.object--item')
            ->each(function (Crawler $node) {
                return new BnSearchResultRow($node);
            });
    }

    /**
     * @return array [(int)$id => (string)$name]
     */
    public function getSubwayStations(): array
    {
        $stations = [];

        $this->crawler()
            ->filter('div.wrapper-popup--metro div.mob_metro_list ul')
            ->each(function (Crawler $ul) use (&$stations) {
                $ul->filter('li.point')->each(function (Crawler $li) use (&$stations) {
                    $id = $li->attr('data-id');
                    $name = trim($li->text());
                    $stations[$id] = $name;
                });
            });

        return $stations;
    }
}