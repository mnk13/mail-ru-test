<?php

declare(strict_types=1);

namespace Bn\Client;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class BnClientException extends \RuntimeException
{
    /**
     * @param GuzzleException|RequestException $exception
     * @return BnClientException
     */
    public static function fromGuzzleException(GuzzleException $exception): self
    {
        $code = $exception->getCode();
        // try to get curl error from handler context
        if ($exception instanceof RequestException) {
            $code = $exception->getHandlerContext()['errno'] ?? $code;
        }

        return new self('Http request failed', $code, $exception);
    }
}
