<?php

declare(strict_types=1);

namespace Bn\Client;

use Symfony\Component\DomCrawler\Crawler;

class BnSearchResultRow implements \JsonSerializable
{
    /** @var Crawler */
    private $node;

    public function __construct(Crawler $domNode)
    {
        $this->node = $domNode;
    }

    public function getRoomsNumber(): ?int
    {
        $title = $this->getText('div.object--title');

        return $title !== null ? (int)$title : null;
    }

    public function getAddress(): ?string
    {
        return $this->getText('div.object--address');
    }

    public function getSubwayStation(): ?string
    {
        return $this->getText('div.object--nearest span.object--metro');
    }

    public function getFloor(): ?string
    {
        return $this->getText('div.object--floor span');
    }

    public function getBuildingType(): ?string
    {
        return $this->getText('div.object--building_type_name');
    }

    public function getTotalArea(): ?float
    {
        $area = $this->getText('div.object--title span.object__square');

        return $area !== null ? (float) $area : null;
    }

    public function getPhone(): ?string
    {
        return $this->getText('div.object--push_phone_inside span');
    }

    public function getContactPerson(): ?string
    {
        return $this->getText('div.object--agency_info');
    }

    public function getMiscInfo(): ?string
    {
        return $this->getText('a.object--description');
    }

    private function getText(string $cssPath): ?string
    {
        $nodes = $this->node->filter($cssPath);
        if (0 === $nodes->count()) {
            return null;
        }

        // replace non breaking spaces and consecutive spaces with one space
        $text = preg_replace('/[\xA0 ]+/u', ' ', $nodes->text());
        $text = trim($text);

        return $text;
    }

    public function toArray(): array
    {
        return [
            'address' => $this->getAddress(),
            'buildingType' => $this->getBuildingType(),
            'contactPerson' => $this->getContactPerson(),
            'floor' => $this->getFloor(),
            'miscInfo' => $this->getMiscInfo(),
            'phone' => $this->getPhone(),
            'roomsNumber' => $this->getRoomsNumber(),
            'subwayStation' => $this->getSubwayStation(),
            'totalArea' => $this->getTotalArea(),
        ];
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }
}
