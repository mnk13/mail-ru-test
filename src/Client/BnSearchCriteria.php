<?php

declare(strict_types=1);

namespace Bn\Client;

class BnSearchCriteria
{
    /** @var int|null */
    private $roomsNumberFrom;
    /** @var int|null */
    private $roomsNumberTo;

    /** @var float|null */
    private $priceFrom;
    /** @var float|null */
    private $priceTo;

    /** @var int[] */
    private $subwayStationIds = [];

    public function withRoomsNumber(?int $from, ?int $to): self
    {
        $copy = clone $this;
        $copy->roomsNumberFrom = $from;
        $copy->roomsNumberTo = $to;

        return $copy;
    }

    public function withPrice(?float $from, ?float $to): self
    {
        $copy = clone $this;
        $copy->priceFrom = $from;
        $copy->priceTo = $to;

        return $copy;
    }

    public function withSubwayStationIds(int ...$ids): self
    {
        $copy = clone $this;
        $copy->subwayStationIds = $ids;

        return $copy;
    }

    /**
     * @return int|null
     */
    public function getRoomsNumberFrom(): ?int
    {
        return $this->roomsNumberFrom;
    }

    /**
     * @return int|null
     */
    public function getRoomsNumberTo(): ?int
    {
        return $this->roomsNumberTo;
    }

    /**
     * @return int[]
     */
    public function getRoomsNumberRange(): array
    {
        if (null === $this->roomsNumberFrom && null === $this->roomsNumberTo) {
            return [];
        }
        if (null !== $this->roomsNumberFrom && null !== $this->roomsNumberTo) {
            return range($this->roomsNumberFrom, $this->roomsNumberTo);
        }
        if (null !== $this->roomsNumberFrom) {
            return [$this->roomsNumberFrom];
        }

        return [$this->roomsNumberTo];
    }

    /**
     * @return float|null
     */
    public function getPriceFrom(): ?float
    {
        return $this->priceFrom;
    }

    /**
     * @return float|null
     */
    public function getPriceTo(): ?float
    {
        return $this->priceTo;
    }

    /**
     * @return int[]
     */
    public function getSubwayStationIds(): array
    {
        return $this->subwayStationIds;
    }
}