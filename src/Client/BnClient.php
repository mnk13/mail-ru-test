<?php

declare(strict_types=1);

namespace Bn\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class BnClient
{
    private const URI = 'https://www.bn.ru/kvartiry-vtorichka/';

    /** @var ClientInterface */
    private $httpClient;

    public function __construct(ClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param BnSearchCriteria|null $criteria
     * @return BnSearchPage
     * @throws BnClientException When request to BN failed
     */
    public function search(BnSearchCriteria $criteria = null): BnSearchPage
    {
        $criteria = $criteria ?? new BnSearchCriteria();
        try {
            $uri = self::URI.'?'.$this->buildSearchQuery($criteria);
            $response = $this->httpClient->request('GET', $uri, [
                RequestOptions::ALLOW_REDIRECTS => false,
                RequestOptions::HTTP_ERRORS => true,
            ]);
        } catch (GuzzleException $exception) {
            throw BnClientException::fromGuzzleException($exception);
        }

        return new BnSearchPage($response->getBody()->getContents(), $uri);
    }

    private function buildSearchQuery(BnSearchCriteria $criteria): string
    {
        $query = [
            'formName' => 'secondary',
            'pricePer' => 1,
        ];
        // price
        if ($criteria->getPriceFrom() !== null) {
            $query['from'] = $criteria->getPriceFrom();
        }
        if ($criteria->getPriceTo() !== null) {
            $query['to'] = $criteria->getPriceTo();
        }
        foreach ($criteria->getRoomsNumberRange() as $roomsNumber) {
            $query['kkv'][] = $roomsNumber;
        }
        foreach ($criteria->getSubwayStationIds() as $stationId) {
            $query['metro'][] = $stationId;
        }

        // replace kkv[0]=1&kkv[2]=2 to kkv[]=1&kkv[]=2
        return preg_replace('/%5B\d+%5D/iu', '%5B%5D', http_build_query($query));
    }
}