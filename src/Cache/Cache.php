<?php

declare(strict_types=1);

namespace Bn\Cache;

class Cache
{
    public function get(string $key, int $ttl, \Closure $callback)
    {
        $value = apcu_fetch($key, $success);
        if ($success) {
            return $value;
        }

        $value = $callback();

        apcu_store($key, $value, $ttl);

        return $value;
    }
}