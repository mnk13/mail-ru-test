<?php

declare(strict_types=1);

namespace Bn;

class Container
{
    /** @var \Closure[] */
    private $definitions;

    /** @var object */
    private $instances;

    public function register(string $class, \Closure $closure = null)
    {
        $closure = $closure ?? function () use ($class) {
            return new $class;
        };

        $this->definitions[$class] = $closure;
    }

    public function get(string $class): object
    {
        if (!isset($this->instances[$class])) {
            $this->instances[$class] = $this->make($class);
        }

        return $this->instances[$class];
    }

    public function make(string $name)
    {
        return $this->definitions[$name]($this);
    }
}