<?php

declare(strict_types=1);

namespace Bn\Form;

use Bn\Client\BnSearchCriteria;

class SearchForm
{
    private const FIELDS = [
        'priceFrom' =>          [FILTER_VALIDATE_FLOAT, []],
        'priceTo' =>            [FILTER_VALIDATE_FLOAT, []],
        'roomsNumberFrom' =>    [FILTER_VALIDATE_INT, []],
        'roomsNumberTo' =>      [FILTER_VALIDATE_INT, []],
        'subwayStationIds' =>   [FILTER_VALIDATE_INT, []],
    ];

    /** @var array */
    private $query;

    public function __construct(array $query)
    {
        $this->query = $query;
    }

    public function validate(): void
    {
        foreach (self::FIELDS as $field => [$filter, $options]) {
            if (!isset($this->query[$field])) {
                continue;
            }

            $isMultiple = is_array($this->query[$field]);
            $values = $isMultiple ? $this->query[$field] : [$this->query[$field]];

            $filtered = [];
            foreach ($values as $value) {
                $result = filter_var($value, $filter, $options);
                if (false === $result) {
                    throw new \InvalidArgumentException(sprintf('Field "%s" has invalid value "%s"', $field, $value));
                }

                $filtered[] = $result;
            }
            $this->query[$field] = $isMultiple ? $filtered : reset($filtered);
        }
    }

    public function createCriteria(): BnSearchCriteria
    {
        $criteria = new BnSearchCriteria();
        $criteria = $criteria->withRoomsNumber($this->query['roomsNumberFrom'] ?? null, $this->query['roomsNumberTo'] ?? null);
        $criteria = $criteria->withPrice($this->query['priceFrom'] ?? null, $this->query['priceTo'] ?? null);
        $criteria = $criteria->withSubwayStationIds(...$this->query['subwayStationIds'] ?? []);

        return $criteria;
    }
}