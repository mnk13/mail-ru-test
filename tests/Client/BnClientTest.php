<?php

declare(strict_types=1);

namespace App\Tests\Bn;

use Bn\Client\BnClient;
use Bn\Client\BnSearchCriteria;
use Bn\Client\BnSearchPage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;

class BnClientTest extends TestCase
{
    /** @var HandlerStack */
    private $handlerStack;

    /** @var BnClient */
    private $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->handlerStack = HandlerStack::create();
        $httpClient = new Client(['handler' => $this->handlerStack]);

        $this->client = new BnClient($httpClient);
    }

    public function testSearch_success(): void
    {
        $body = $this->readFixture('search.response.html');
        $this->mockHttpHandler(new Response(200, [], $body));

        $result = $this->client->search();

        self::assertInstanceOf(BnSearchPage::class, $result);

        $rows = $result->getSearchRows();

        self::assertCount(30, $rows);

        $expectedRow = [
            'address' => 'Ленинградская область, пос. Колтуши, дер. Старая, Иоанна Кронштадтского ул., 1, 3, 5',
            'buildingType' => 'Панельный',
            'contactPerson' => 'Колтушская Строительная Компания (КСК)',
            'floor' => '6/12',
            'phone' => '+7 (812) XXX-XX-77',
            'roomsNumber' => 1,
            'subwayStation' => 'Улица Дыбенко',
            'totalArea' => 25.63,
        ];

        $row = $rows[0]->toArray();
        // remove misc info it too big
        unset($row['miscInfo']);

        self::assertEquals($expectedRow, $row);
    }

    public function testSearch_miscInfo(): void
    {
        $body = $this->readFixture('search.response.html');
        $this->mockHttpHandler(new Response(200, [], $body));

        $result = $this->client->search();

        self::assertInstanceOf(BnSearchPage::class, $result);

        $miscInfo = $result->getSearchRows()[0]->getMiscInfo();

        $expected = <<<EOE
Квартира в строящемся комплексе "Центральный" в Колтушах. 15 минут до метро ул. Дыбенко, хорошая экология, вся инфраструктура. 500 м. от Колтушского ш. с выездом на КАД. Строительство полным ходом. В микрорайон входят: 12-ти и 16-ти этажные жилые дома, Отдельный 5-ти этажный паркинг, Детский сад, Автономная газовая котельная: отопление и горячее водоснабжение. Грузопассажирские лифты Welmax.Благоустройство: детские игровые и спортивные площадки, оригинальное озеленение, альпийские горки и т. п. Первые этажи зданий: детские образовательные учреждения,магазины,салоны красоты, SPA салоны, медицинские кабинеты. Возможна ипотека.
EOE;

        self::assertSame($expected, $miscInfo);
    }

    /**
     * @dataProvider requestCriteriaProvider
     *
     * @param BnSearchCriteria $criteria
     * @param string $expectedUri
     */
    public function testSearch_requestWithCriteria(BnSearchCriteria $criteria, string $expectedUri): void
    {
        $body = $this->readFixture('search.response.html');
        $history = $this->mockHttpHandler(new Response(200, [], $body));

        $this->client->search($criteria);

        self::assertCount(1, $history, 'One request should be sent');
        /** @var RequestInterface $request */
        $request = $history[0]['request'];
        self::assertSame(
            $expectedUri,
            (string) $request->getUri()
        );
        self::assertSame('GET', $request->getMethod());
    }

    public static function requestCriteriaProvider(): array
    {
        return [
            'all params' => [
                'criteria' => (new BnSearchCriteria())
                    ->withPrice(1000.0, 3000.0)
                    ->withRoomsNumber(1, 3)
                    ->withSubwayStationIds(500, 1000),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&from=1000&to=3000&kkv%5B%5D=1&kkv%5B%5D=2&kkv%5B%5D=3&metro%5B%5D=500&metro%5B%5D=1000',
            ],
            'empty criteria' => [
                'criteria' => new BnSearchCriteria(),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1',
            ],
            'only to price' => [
                'criteria' => (new BnSearchCriteria())
                    ->withPrice(null, 3000.0),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&to=3000',
            ],
            'only from price' => [
                'criteria' => (new BnSearchCriteria())
                    ->withPrice(1000.0, null),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&from=1000',
            ],
            'only rooms number from' => [
                'criteria' => (new BnSearchCriteria())
                    ->withRoomsNumber(1, null),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&kkv%5B%5D=1',
            ],
            'only rooms number to' => [
                'criteria' => (new BnSearchCriteria())
                    ->withRoomsNumber(null, 3),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&kkv%5B%5D=3',
            ],
            'rooms number to and from same' => [
                'criteria' => (new BnSearchCriteria())
                    ->withRoomsNumber(2, 2),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&kkv%5B%5D=2',
            ],
            'rooms number to and from range' => [
                'criteria' => (new BnSearchCriteria())
                    ->withRoomsNumber(1, 4),
                'expectedUri' => 'https://www.bn.ru/kvartiry-vtorichka/?formName=secondary&pricePer=1&kkv%5B%5D=1&kkv%5B%5D=2&kkv%5B%5D=3&kkv%5B%5D=4',
            ],
        ];
    }

    /**
     * @expectedException \Bn\Client\BnClientException
     * @expectedExceptionMessage Http request failed
     * @expectedExceptionCode 404
     */
    public function testSearch_requestFailed404(): void
    {
        $this->mockHttpHandler(new Response(404));

        $this->client->search();
    }

    /**
     * @expectedException \Bn\Client\BnClientException
     * @expectedExceptionMessage Http request failed
     * @expectedExceptionCode 502
     */
    public function testSearch_requestFailed502(): void
    {
        $this->mockHttpHandler(new Response(502));

        $this->client->search();
    }

    /**
     * @expectedException \Bn\Client\BnClientException
     * @expectedExceptionMessage Http request failed
     * @expectedExceptionCode 6
     */
    public function testSearch_requestFailedResolveHost(): void
    {
        $curlException = new ConnectException(
            'cURL error 6: Could not resolve host: www.bn.ru (see http://curl.haxx.se/libcurl/c/libcurl-errors.html)',
            new Request('GET', 'https://www.bn.ru/'),
            null,
            [ // Curl error number and text
                'errno' => 6,
                'error' => 'Could not resolve host: www.bn.ru',
            ]
        );

        $this->mockHttpHandler($curlException);

        $this->client->search();
    }

    public function testSearch_dummyHtml(): void
    {
        $this->mockHttpHandler(new Response(200, [], '<html><body><h1>Ooops!</h1></body>'));

        $result = $this->client->search();

        self::assertCount(0, $result->getSearchRows());
    }

    public function testSearch_getSubwayStations(): void
    {
        $body = $this->readFixture('search.response.html');
        $this->mockHttpHandler(new Response(200, [], $body));

        $result = $this->client->search();

        $stations = $result->getSubwayStations();

        self::assertCount(67, $stations);

        // check first 5 stations
        $expected = [
            500 => 'Автово',
            1190 => 'Адмиралтейская',
            510 => 'Академическая',
            520 => 'Балтийская',
            1230 => 'Беговая',
        ];
        self::assertEquals($expected, array_slice($stations, 0, 5, true));
    }

    private function mockHttpHandler(...$queue): \ArrayObject
    {
        $history = new \ArrayObject();
        $this->handlerStack->push(Middleware::history($history));
        $this->handlerStack->setHandler(new MockHandler($queue));

        return $history;
    }

    private function readFixture(string $fixturePath): string
    {
        return file_get_contents(__DIR__.'/../fixtures/'.$fixturePath);
    }
}
