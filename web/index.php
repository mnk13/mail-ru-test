<?php

declare(strict_types=1);

use Bn\Cache\Cache;
use Bn\Client\BnClient;
use Bn\Form\SearchForm;

/** @var \Bn\Container $container */
require __DIR__ . '/../vendor/autoload.php';

$container = require __DIR__.'/../app/app.php';

//phpinfo();

define('STATUS_CODES', [
    200 => 'OK',
    400 => 'Bad Request',
    405 => 'Method Not Allowed',
    500 => 'Internal Server Error',
]);

$requestUri = $_SERVER['PATH_INFO'];
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ('/search' === $requestUri && 'GET' === $requestMethod) {
    $action = function () use ($container): array {
        /** @var BnClient $bnClient */
        $bnClient = $container->get(BnClient::class);

        $form = new SearchForm($_GET);
        $form->validate();

        $page = $bnClient->search($form->createCriteria());

        return [200, [], $page->getSearchRows()];
    };
} elseif ('/subway-stations' === $requestUri && 'GET' === $requestMethod) {
    $action = function() use ($container): array {
        /** @var BnClient $bnClient */
        $bnClient = $container->get(BnClient::class);
        /** @var Cache $cache */
        $cache = $container->get(Cache::class);
        $stations = $cache->get('subway-stations', 3600, function () use ($bnClient) {
            return $bnClient->search()->getSubwayStations();
        });

        return [200, [], $stations];
    };
} else {
    $action = function(): array {
        return [405, [], ['error' => 'Method is not allowed']];
    };
}

$onError = function(\Throwable $exception): array {
    if ($exception instanceof \InvalidArgumentException) {
        $code = 400;
    } else {
        $code = 500;
    }

    $json = ['code' => $code, 'error' => $exception->getMessage()];

    return [$code, [], $json];
};

// Dispatch http action
try {
    [$statusCode, $headers, $body] = $action();
} catch (\Throwable $exception) {
    [$statusCode, $headers, $body] = $onError($exception);
}

// if body is array its should be serialized to json and served as json
if (is_array($body)) {
    $body = json_encode($body, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    $headers['Content-Type'] = $headers['Content-Type'] ?? 'application/json';
}

// Server http response
foreach ($headers as $header => $value) {
    header($header.': '.$value, false, $statusCode);
}
header(sprintf('HTTP/1.0 %s %s', $statusCode, STATUS_CODES[$statusCode] ?? ''), true, $statusCode);
echo $body;
