<?php

declare(strict_types=1);

use Bn\Cache\Cache;
use Bn\Client\BnClient;
use Bn\Container;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\RequestOptions;

$container = new Container();

$container->register(ClientInterface::class, function () {
    $handler = HandlerStack::create();
    $config = [
        RequestOptions::HEADERS => ['Accept-Encoding' => 'gzip'],
        RequestOptions::TIMEOUT => 60,
        'handler' => $handler,
    ];

    return new Client($config);
});

$container->register(BnClient::class, function (Container $container) {
    return new BnClient($container->get(ClientInterface::class));
});

$container->register(Cache::class);

return $container;